package io.srm.service;

import io.srm.Application;
import io.srm.domain.dto.CustomerDTO;
import io.srm.domain.dto.CustomerDTOBuilder;
import io.srm.domain.entity.Customer;
import io.srm.domain.entity.CustomerBuilder;
import io.srm.domain.enums.Risk;
import io.srm.exception.BusinessException;
import io.srm.exception.NotFoundException;
import io.srm.mapper.CustomerMapper;
import io.srm.repository.CustomerRepository;
import io.vavr.control.Either;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class CustomerServiceImplTest {

    @MockBean
    private CustomerRepository repository;

    @MockBean
    private CustomerMapper mapper;

    @Autowired
    private CustomerService service;

    @Test
    public void findAll() throws Exception {
        List<Customer> entities = getEntities();

        when(repository.findAll()).thenReturn(entities);
        when(mapper.toDto(entities)).thenReturn(Optional.of(getDtos()));

        Optional<Collection<CustomerDTO>> customers = service.findAll();

        assertEquals(3, customers.get().size());
    }

    @Test(expected = BusinessException.class)
    public void saveOrUpdate_entity_already_exists() throws Throwable {
        CustomerDTO dto = getDTO();
        Customer entity = getEntity();

        when(mapper.toEntity(dto)).thenReturn(Optional.of(entity));
        when(repository.findByName(entity.getName())).thenReturn(entity);

        Either<Throwable, Optional<CustomerDTO>> result = service.saveOrUpdate(dto);

        if (result.isLeft()) throw result.getLeft();
    }

    @Test(expected = BusinessException.class)
    public void saveOrUpdate_dto_invalid() throws Throwable {
        CustomerDTO c = new CustomerDTO();

        Either<Throwable, Optional<CustomerDTO>> result = service.saveOrUpdate(c);

        if (result.isLeft()) throw result.getLeft();
    }

    @Test(expected = NotFoundException.class)
    public void saveOrUpdate_not_found() throws Throwable {
        CustomerDTO dto = getDTO();
        dto.setId(1L);

        Customer entity = getEntity();
        entity.setId(1L);

        when(mapper.toEntity(dto)).thenReturn(Optional.of(entity));
        when(repository.findByName(Mockito.any())).thenReturn(null);
        when(repository.findOne(1L)).thenReturn(null);

        Either<Throwable, Optional<CustomerDTO>> result = service.saveOrUpdate(dto);

        if (result.isLeft()) throw result.getLeft();
    }

    @Test
    public void saveOrUpdate_create_success() {
        CustomerDTO dto = getDTO();
        Customer entity = getEntity();

        when(mapper.toEntity(dto)).thenReturn(Optional.of(entity));
        when(repository.findByName(entity.getName())).thenReturn(null);
        when(mapper.toDto(entity)).thenReturn(Optional.of(dto));
        when(repository.saveAndFlush(entity)).thenReturn(entity);

        Either<Throwable, Optional<CustomerDTO>> result = service.saveOrUpdate(dto);

        assertTrue(result.isRight());
    }

    @Test
    public void saveOrUpdate_update_success() {
        CustomerDTO dto = getDTO();
        dto.setId(1L);

        Customer entity = getEntity();
        entity.setId(1L);

        when(mapper.toEntity(dto)).thenReturn(Optional.of(entity));
        when(repository.findOne(1L)).thenReturn(entity);
        when(repository.findByName(entity.getName())).thenReturn(null);
        when(mapper.toDto(entity)).thenReturn(Optional.of(dto));
        when(repository.saveAndFlush(entity)).thenReturn(entity);

        Either<Throwable, Optional<CustomerDTO>> result = service.saveOrUpdate(dto);

        assertTrue(result.isRight());
    }

    private List<Customer> getEntities() {
        return
                Arrays.asList(
                        new CustomerBuilder()
                                .setCreditLimit(12.0)
                                .setId(1L)
                                .setName("Marcos")
                                .setRisk(Risk.A)
                                .build(),
                        new CustomerBuilder()
                                .setCreditLimit(13.0)
                                .setId(2L)
                                .setName("Roberto")
                                .setRisk(Risk.B)
                                .build(),
                        new CustomerBuilder()
                                .setCreditLimit(14.0)
                                .setId(3L)
                                .setName("Lucca")
                                .setRisk(Risk.C)
                                .build()
                );
    }

    private List<CustomerDTO> getDtos() {
        return
                Arrays.asList(
                        new CustomerDTOBuilder()
                                .setCreditLimit(12.0)
                                .setId(1L)
                                .setName("Marcos")
                                .setRisk(Risk.A)
                                .build(),
                        new CustomerDTOBuilder()
                                .setCreditLimit(13.0)
                                .setId(2L)
                                .setName("Roberto")
                                .setRisk(Risk.B)
                                .build(),
                        new CustomerDTOBuilder()
                                .setCreditLimit(14.0)
                                .setId(3L)
                                .setName("Lucca")
                                .setRisk(Risk.C)
                                .build()
                );
    }

    private Customer getEntity() {
        return
                new CustomerBuilder()
                        .setCreditLimit(12.0)
                        .setName("Marcos")
                        .setRisk(Risk.A)
                        .build();
    }

    private CustomerDTO getDTO() {
        return
                new CustomerDTOBuilder()
                        .setCreditLimit(12.0)
                        .setName("Marcos")
                        .setRisk(Risk.A)
                        .build();
    }

}