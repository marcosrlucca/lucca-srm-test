package io.srm.service;

import io.srm.domain.dto.CustomerDTO;
import io.vavr.control.Either;

import java.util.Collection;
import java.util.Optional;

public interface CustomerService {

    Optional<Collection<CustomerDTO>> findAll();

    Either<Throwable, Optional<CustomerDTO>> saveOrUpdate(CustomerDTO dto);
}