package io.srm.domain.dto;

import io.srm.domain.enums.Risk;

import java.util.Objects;

public class CustomerDTO {
    private Long id;
    private String name;
    private Double creditLimit;
    private Double interestRate;
    private Risk risk;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(Double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public Double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(Double interestRate) {
        this.interestRate = interestRate;
    }

    public Risk getRisk() {
        return risk;
    }

    public void setRisk(Risk risk) {
        this.risk = risk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerDTO that = (CustomerDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(creditLimit, that.creditLimit) &&
                Objects.equals(interestRate, that.interestRate) &&
                risk == that.risk;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, creditLimit, interestRate, risk);
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", creditLimit=" + creditLimit +
                ", interestRate=" + interestRate +
                ", risk=" + risk +
                '}';
    }
}
