package io.srm.domain.enums;

public interface IStringEnum<T> {

    T getValue();

}
