package io.srm.domain.entity;

import io.srm.domain.enums.Risk;

public class CustomerBuilder {
    private Long id;
    private String name;
    private Double creditLimit;
    private Double interestRate;
    private Risk risk;

    public CustomerBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public CustomerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CustomerBuilder setCreditLimit(Double creditLimit) {
        this.creditLimit = creditLimit;
        return this;
    }

    public CustomerBuilder setInterestRate(Double interestRate) {
        this.interestRate = interestRate;
        return this;
    }

    public CustomerBuilder setRisk(Risk risk) {
        this.risk = risk;
        return this;
    }

    public Customer build() {
        Customer c = new Customer();
        c.setInterestRate(interestRate);
        c.setRisk(risk);
        c.setCreditLimit(creditLimit);
        c.setName(name);
        c.setId(id);
        return c;
    }
}
