package io.srm.controller;

import io.srm.domain.dto.CustomerDTO;
import io.srm.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/customer", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerController {

    private final static Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    private final CustomerService service;

    @Autowired
    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity getAll() {
        LOGGER.info("getting all customers");

        final Optional<Collection<CustomerDTO>> maybeResult = service.findAll();

        return maybeResult
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.noContent().build());
    }

    @PostMapping
    public ResponseEntity saveOrUpdate(@RequestBody CustomerDTO dto) throws Throwable {
        LOGGER.info("saving a customer");

        if (dto == null) {
            LOGGER.info("the customer is null");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return service.saveOrUpdate(dto)
                .mapLeft(e -> e)
                .map(ResponseEntity::ok)
                .getOrElseThrow(e -> e);
    }
}