import {NgModule} from '@angular/core';

import {NgbAlertModule, NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/modules/shared.module';
import {
    MatButtonModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatTableModule
} from '@angular/material';
import {SRMRoutingModule} from './srm-routing.module';
import {SRMComponent} from './srm.component';
import {CustomerComponent} from './customer/customer.component';
import {CustomerService} from './customer/customer.service';

@NgModule({
    imports:
        [
            SharedModule,
            FormsModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            MatTableModule,
            MatSelectModule,
            NgbCarouselModule.forRoot(),
            NgbAlertModule.forRoot(),
            SRMRoutingModule,
            ReactiveFormsModule
        ],
    providers:
        [
            CustomerService
        ],
    declarations:
        [
            SRMComponent,
            CustomerComponent
        ]
})
export class SRMModule {
}
