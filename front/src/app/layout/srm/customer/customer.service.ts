import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Customer} from '../model/srm-form.model';

@Injectable()
export class CustomerService {

    private _api_url = `${environment._api_server_url}/api/customer`;

    constructor(private httpClient: HttpClient) {
    }

    findAll() {
        return this.httpClient.get(this._api_url);
    }

    save(currentCustomer: Customer) {
        return this.httpClient.post(this._api_url, currentCustomer);
    }

    getRisks() {
        // noinspection TypeScriptUnresolvedFunction
        return Observable.of(
            {value: 'A', viewValue: 'A'},
            {value: 'B', viewValue: 'B'},
            {value: 'C', viewValue: 'C'}
        );
    }
}
