import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {Customer} from '../model/srm-form.model';
import {CustomerService} from './customer.service';

export class CustomerDataSource implements DataSource<Customer> {

    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public data = new BehaviorSubject<Customer[]>([]);

    constructor(private service: CustomerService) {
    }

    connect(collectionViewer: CollectionViewer): Observable<Customer[]> {
        return this.data.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.data.complete();
        this.loadingSubjects.complete();
    }

    loadData() {
        this.loadingSubjects.next(true);

        this.service.findAll()
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false))
            )
            .subscribe((response: Customer[]) => {
                this.data.next(response);
            });
    }
}
