import {AfterViewInit, Component, OnInit, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {ToastsManager} from 'ng2-toastr';
import {Customer} from '../model/srm-form.model';
import {CustomerService} from './customer.service';
import {CustomerDataSource} from './customer.data-source';
import {HttpStatus} from '../../../shared/domain/http-status';

@Component({
    selector: 'app-customer',
    templateUrl: './customer.component.html',
    styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit, AfterViewInit {

    dataSource: CustomerDataSource;
    displayedColumns = ['name', 'creditLimit', 'risk'];
    highlightedRows = [];
    risks = [];
    risk;
    selectedRisk;
    form: FormGroup;
    currentCustomer: Customer;
    labelForm = 'Cadastrar';

    constructor(private service: CustomerService, private formBuilder: FormBuilder, private toastr: ToastsManager, vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.dataSource = new CustomerDataSource(this.service);

        this.form = this.formBuilder.group({
            name: new FormControl('', [Validators.required]),
            creditLimit: new FormControl('', [Validators.required]),
        });

        this.risk = new FormControl('', [Validators.required]);

        this.currentCustomer = new Customer();

        this.populateSelect();
    }

    ngAfterViewInit() {
        this.loadData();
    }

    populateSelect() {
        this.service.getRisks().subscribe((item) => this.risks.push(item));
    }

    private loadData() {
        this.dataSource = new CustomerDataSource(this.service);

        this.dataSource.loadData();
    }

    onSelectRow(row: Customer) {
        const index = this.highlightedRows.findIndex((a: Customer) => a.id === row.id);

        if (index === -1 && this.highlightedRows.length < 1) { // just one by time
            this.highlightedRows.push(row);
        } else {
            this.highlightedRows.splice(index, 1);
            this.highlightedRows.push(row);
        }

        this.currentCustomer = row;

        this.labelForm = 'Editar';
    }

    save(formDirective: FormGroupDirective) {
        const formIsValid: boolean = this.validate();

        if (!formIsValid) {
            return;
        }

        this.currentCustomer.risk = this.selectedRisk;

        this.service.save(this.currentCustomer).subscribe(
            () => {
                this.dataSource.loadData();

                this.reset(formDirective);

                setTimeout(() => {
                    this.toastr.info('Cliente salvo com sucesso', null, {toastLife: 2000});
                }, 500);
            }, (error) => {
                if (error.status === HttpStatus.INTERNAL_SERVER_ERROR) {
                    this.toastr.error('Comportamente inesperado', null, {toastLife: 2000, showCloseButton: true});
                }
            }
        );
    }

    onSubmit(formData: any, formDirective: FormGroupDirective) {
        this.save(formDirective);
    }

    private validate() {
        let isValid = true;

        if (isNullOrUndefined(this.currentCustomer)) {
            this.toastr.warning('Campos incorretos', null, {toastLife: 2000});
            isValid = false;
        }
        if (isNullOrUndefined(this.currentCustomer.name) || this.currentCustomer.name === '') {
            this.toastr.warning('O campo Nome deve ser preenchido', null, {toastLife: 2000});
            isValid = false;
        }
        if (isNullOrUndefined(this.currentCustomer.creditLimit) || this.currentCustomer.creditLimit === '') {
            this.toastr.warning('O campo Limite de Crédito deve ser preenchido', null, {toastLife: 2000});
            isValid = false;
        }
        if (isNullOrUndefined(this.selectedRisk)) {
            this.toastr.warning('O campo Riscos deve ser preenchido', null, {toastLife: 2000});
            isValid = false;
        }

        return isValid;
    }

    reset(formDirective: FormGroupDirective) {
        this.form = this.formBuilder.group({
            name: new FormControl('', [Validators.required]),
            creditLimit: new FormControl('', [Validators.required]),
        });

        this.risk = new FormControl('', [Validators.required]);

        this.highlightedRows = [];

        this.labelForm = 'Cadastrar';

        if (!isNullOrUndefined(formDirective)) {
            formDirective.resetForm();
        }

        this.currentCustomer = new Customer();
    }
}
