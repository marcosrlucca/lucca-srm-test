import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: [
        './app.component.scss',
        '../../node_modules/@angular/material/prebuilt-themes/indigo-pink.css'
    ],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    constructor() {
    }

    ngOnInit() {
    }
}
