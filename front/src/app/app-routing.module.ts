import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: './layout/layout.module#LayoutModule'
    },
    {
        path: 'nao-encontrado',
        loadChildren: './not-found/not-found.module#NotFoundModule'
    },
    {
        path: '**',
        redirectTo: 'nao-encontrado'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
